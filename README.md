## Submission
* The submission is based on the challenge available [here](https://gitlab.com/snaptravel/connectivity-demo/)
* The submission is built on python 3.7 and MongoDB
* Please run the command
```
pip install -r requirements
```
* I have made the assumption that we do not store duplicates in database. To that effect, please create an index on the `id` field in MongoDB using the command
```
db.hotels.createIndex({"id": 1}, {unique: true})
```
under the **connectivity_demo** database locally.
* If the index is not created, duplicates will be stored in the database. In either case, error handling is done to make sure program doesnt crash.
* The 2 (only) test cases can be run by running the file **unittest.py**