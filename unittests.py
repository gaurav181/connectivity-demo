import unittest
import service
from unittest.mock import MagicMock


TEST_UNMERGED_DATA = [
    ('json', [{'id': 1, 'hotel_name': 'Test1', 'num_reviews': 27, 'price': 50},
              {'id': 2, 'hotel_name': 'Test2', 'num_reviews': 28, 'price': 550},
              {'id': 4, 'hotel_name': 'Test4', 'num_reviews': 29, 'price': 20}]),
    ('xml', [{'id': '1', 'hotel_name': 'Test1', 'num_reviews': 27, 'price': 30},
              {'id': '3', 'hotel_name': 'Test3', 'num_reviews': 8, 'price': 40},
              {'id': '4', 'hotel_name': 'Test4', 'num_reviews': 29, 'price': 20}])
]

TEST_MERGED_DATA = [
    {'id': 1, 'hotel_name': 'Test1', 'num_reviews': 27, 'prices': {'snaptravel': 50, 'retail': 30}},
    {'id': 4, 'hotel_name': 'Test4', 'num_reviews': 29, 'prices': {'snaptravel': 20, 'retail': 20}}
]


class MockParallelRequestsTest(unittest.TestCase):
    def test_mock_requests(self):
        HDFT = service.HotelDataFetcher('TestCity', '99/99/1199', '99/99/9999')
        HDFT.get_parallel_post_request_responses = MagicMock(return_value = TEST_UNMERGED_DATA)
        return_data = HDFT.get_parallel_post_request_responses()
        for i in [0,1]:
            self.assertEqual(return_data[i][0], TEST_UNMERGED_DATA[i][0])
            self.assertTrue(len(return_data[i][1])==3)

class DataMergeTest(unittest.TestCase):
    def test_data_merging(self):
        results = service.get_merged_hotel_info(TEST_UNMERGED_DATA)
        for i in [0,1]:
            self.assertEqual(results[i]['id'], TEST_MERGED_DATA[i]['id'])
            self.assertEqual(results[i]['hotel_name'], TEST_MERGED_DATA[i]['hotel_name'])


if __name__ == '__main__':
    unittest.main()