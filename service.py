import argparse
import dicttoxml
import json
import requests
from multiprocessing import Pool
from requests.exceptions import HTTPError
import xmldict
import pymongo

JSON_REQUEST_CONTEXT = {
        'URL' : 'https://experimentation.getsnaptravel.com/interview/hotels',
        'HEADERS' : {'Content-type' : 'application/json; charset=utf-8'}
    }

XML_REQUEST_CONTEXT = {
    'URL': 'https://experimentation.getsnaptravel.com/interview/legacy_hotels',
    'HEADERS': {'Content-type': 'application/xml; charset=utf-8'}
}

class HotelDataFetcher:

    def __init__(self, city, checkin, checkout, provider = 'snaptravel'):
        self.city = city
        self.checkin = checkin
        self.checkout = checkout
        self.provider = provider

    def make_post_request(self, content_type):
        if content_type == 'json':
            request_context = JSON_REQUEST_CONTEXT
            request_payload = self.generate_json_payload()
        else:
            request_context = XML_REQUEST_CONTEXT
            request_payload = self.generate_xml_payload()

        try:
            response = requests.post(url=request_context['URL'], data=request_payload,
                                 headers=request_context['HEADERS'])
            if content_type == 'json':
                results = ('json', json.loads(response.content.decode('utf-8'))['hotels'])
            else:
                results = ('xml', xmldict.xml_to_dict(response.content.decode('utf-8'))['root']['element'])
            return results
        except HTTPError as http_err:
            print('HTTP error occurred: {}'.format(http_err))
        except Exception as err:
            print(err.message)
        return None

    def get_parallel_post_request_responses(self):
        pool = Pool(2)
        results = pool.map(self.make_post_request, ['json', 'xml'])
        pool.close()
        return results

    def generate_json_payload(self):
        return json.dumps(self.__dict__)

    def generate_xml_payload(self):
        xml_payload = dicttoxml.dicttoxml(self.__dict__, attr_type=False)
        return str(xml_payload, 'utf-8')

def get_merged_hotel_info(resultsets):
    data = {}
    xml_hotel_dict = {}
    final_hotel_list = []
    for (content_type, hotels) in resultsets:
        data[content_type] = hotels

    for hotel in data['xml']:
        xml_hotel_dict[hotel['id']] = hotel

    for hotel in data['json']:
        if str(hotel['id']) in xml_hotel_dict:
            hotel['prices'] = {
                'retail' : xml_hotel_dict[str(hotel['id'])]['price'],
                'snaptravel' : hotel['price']
            }
            del(hotel['price'])
            final_hotel_list.append(hotel)

    return final_hotel_list

def store_results_to_db(hotel_list_data):
    results = [] #for future error handling
    client = pymongo.MongoClient()
    db = client['connectivity_demo']
    try:
        for hotel in hotel_list_data:
            result = db.hotels.insert_one(hotel)
            results.append(result)
    except pymongo.errors.DuplicateKeyError:
        print ('Hotel with id:'+ str(hotel['id']) + ' not added to database since it already existed')

    return results

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--city')
    parser.add_argument('--checkin')
    parser.add_argument('--checkout')
    return parser.parse_args()

def main():
    args = get_args()

    HDF = HotelDataFetcher(args.city, args.checkin, args.checkout)
    results = HDF.get_parallel_post_request_responses()
    merged_results = get_merged_hotel_info(results)
    store_results_to_db(merged_results)

if __name__ == '__main__':
    main()